const navToggleBtn=document.querySelector(".navbar-nav-toggle");
const navbarNav=document.querySelector(".navbar-nav");
const subMenuToggleBtn=document.querySelectorAll(".navbar-nav .sub-menu-toggle");

navToggleBtn.addEventListener("click",function () {
	if(navbarNav.classList.contains("open")){
		navbarNav.classList.remove("open");
		navbarNav.removeAttribute("style");
		hideSubMenu();
	}
	else{
	 navbarNav.classList.add("open");
	 navbarNav.style.maxHeight=	navbarNav.scrollHeight + "px";
	}

	navToggleBtn.classList.toggle("close");
})

for(let i=0; i<subMenuToggleBtn.length; i++){
   subMenuToggleBtn[i].addEventListener("click",function(){
	if(window.innerWidth < 768){
	   const dropdown=this.parentElement;
	   const height=dropdown.querySelector(".sub-menu").scrollHeight;
	   const subMenu=dropdown.querySelector(".sub-menu");
	   // console.log(height)
	   if(subMenu.classList.contains("open")){
		   // if subMenu classList has class open then
		   subMenu.classList.remove("open");
		   subMenu.removeAttribute("style");
		   navbarNav.style.maxHeight=(navbarNav.scrollHeight - height) + "px";
	   }
	  else{
		// if subMenu classList has no class open then
		subMenu.classList.add("open")
		subMenu.style.maxHeight=height + "px";
		navbarNav.style.maxHeight=(navbarNav.scrollHeight + height) + "px";
	  }
	}
	
   })
}

function hideSubMenu(){
	for(let i=0; i<subMenuToggleBtn.length; i++){
		const dropdown=subMenuToggleBtn[i].parentElement;
		dropdown.querySelector(".sub-menu").removeAttribute("style");
		dropdown.querySelector(".sub-menu").classList.remove("open");
}
}

window.addEventListener("resize" , function(){
	   navbarNav.classList.remove("open");
		navbarNav.removeAttribute("style");
		hideSubMenu();
		navToggleBtn.classList.remove("close");
})





 const slides=document.querySelector(".slider").children;
 const prev=document.querySelector(".prev");
 const next=document.querySelector(".next");
 const indicator=document.querySelector(".indicator");
 let index=0;
 
 prev.addEventListener("click", function(){
	 prevSlide();
 })
 
 next.addEventListener("click", function(){
	 nextSlide();
	 resetTimer();
 })
 
 
 function prevSlide(){
	 if(index==0){
		 index=slides.length-1;
	 }
	 else{
		 index--;
	 }
	 changeSlide();
 }
 
 function nextSlide(){
	 if(index==slides.length-1){
		 index=0;
	 }
	 else{
		 index++;
	 }
	 
	 changeSlide();
 }
 
 function changeSlide(){
		 for( i=0; i<slides.length; i++){
			 slides[i].classList.remove("active");
		 }
 
	 slides[index].classList.add("active");
 }
 
 function resetTimer(){
	 clearInterval(timer);
	 timer=setInterval(autoplay,10000);
 }
 
 function autoplay(){
	 nextSlide();
 }
 
 let timer=setInterval(autoplay,10000);
 


 
var slideIndex = 0;
showSlides();

function showSlides() {
var i;
var slides = document.getElementsByClassName("mySlides");
var dots = document.getElementsByClassName("dot");
for (i = 0; i < slides.length; i++) {
	slides[i].style.display = "none";  
}
slideIndex++;
if (slideIndex > slides.length) {slideIndex = 1}    
for (i = 0; i < dots.length; i++) {
	dots[i].className = dots[i].className.replace(" active", "");
}
slides[slideIndex-1].style.display = "block";  
dots[slideIndex-1].className += " active";
setTimeout(showSlides, 6000); // Change image every 2 seconds
}    