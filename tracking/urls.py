from django.urls import path

from . import views

urlpatterns = [
    path('track/', views.ParcelListView.as_view(), name='track'),
    path('track/<slug>/', views.ParcelDetailView.as_view(), name='parcel_detail'),
]