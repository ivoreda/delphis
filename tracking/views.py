from django.shortcuts import render
from django.views.generic import ListView, DetailView
from . import models
from django.db.models import Q

# Create your views here.

class ParcelListView(ListView):
    model = models.Parcel
    template_name = 'parcel_list.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = models.Parcel.objects.filter(tracking_number__exact=query)
        return object_list


class ParcelDetailView(DetailView):
    model = models.Parcel
    template_name = 'parcel_detail.html'

    # def get_queryset(self):
    #     query = self.request.GET.get('q')
    #     object_list = models.Parcel.objects.filter(
    #         Q(tracking_number__icontains=query)
    #     )
    #     return object_list