from django.contrib import admin
from . import models
# Register your models here.

@admin.register(models.Parcel)
class ParcelAdmin(admin.ModelAdmin):
    list_display = ('tracking_number', 'status', 'ordered_date', 'origin', 'destination')
    list_filter = ('status', 'ordered_date')
    search_fields = ('tracking_number', 'origin', 'destination')
    prepopulated_fields = {'slug': ('tracking_number',)}
    raw_id_field = ('name_of_sender',)
    date_hierarchy = 'ordered_date'
    ordering = ('status', 'ordered_date')