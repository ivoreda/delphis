from django.db import models

# Create your models here.
class Parcel(models.Model):
    STATUS_CHOICES = (
                    ('Delivered', 'Delivered'),
                    ('In Transit', 'In Transit'),
                    ('Pending', 'Pending')
    )

    tracking_number = models.CharField(max_length=9)
    slug = models.SlugField(max_length=9)
    parcel_weight = models.FloatField()
    number_of_parcels = models.CharField(max_length=20)
    origin = models.CharField(max_length=20)
    origin_state = models.CharField(max_length=20)
    location = models.CharField(max_length=20)
    location_state = models.CharField(max_length=20)
    destination = models.CharField(max_length=20)
    destination_state = models.CharField(max_length=20)
    status = models.CharField(choices=STATUS_CHOICES, default='In Transit', max_length=15)
    sender_name = models.CharField(max_length=20)
    sender_email = models.EmailField()
    sender_number = models.CharField(max_length=20)
    recipient_name = models.CharField(max_length=20)
    recipient_email = models.EmailField()
    recipient_number = models.CharField(max_length=20)
    ordered_date = models.DateTimeField(auto_now_add=True)
    delivery_date = models.DateField(auto_now=False)
    delivery_date.editable=True

    class Meta:
        ordering = ('-ordered_date',)

    def __str__(self):
        return self.tracking_number