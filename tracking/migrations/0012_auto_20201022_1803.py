# Generated by Django 3.1.2 on 2020-10-22 17:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tracking', '0011_auto_20201022_1759'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcel',
            name='parcel_weight',
            field=models.FloatField(),
        ),
    ]
